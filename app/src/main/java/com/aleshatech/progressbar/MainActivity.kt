package com.aleshatech.progressbar

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.ProgressBar

// hello i am developer/
// 3rd update

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val button: Button = findViewById(R.id.buttonId)
        val progressBar: ProgressBar = findViewById(R.id.progressBar)

        button.setOnClickListener() {
            button.visibility = View.GONE
            progressBar.visibility = View.VISIBLE

            val intent = Intent(this, MainActivity2::class.java)
            startActivity(intent)

        }
    }
}